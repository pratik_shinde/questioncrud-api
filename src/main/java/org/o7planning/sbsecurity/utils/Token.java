package org.o7planning.sbsecurity.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class Token {
	@Autowired
	JdbcTemplate jdbcTemplate;
	String token;
	String user_id;
	String user_name;
	String testType;

	public String genToken() throws SQLException {
		StringBuilder temp_token = new StringBuilder("");
		temp_token.append("CDKTEST" + testType.charAt(0) + user_id);
		token= temp_token.toString();
//		return this.getUser_name()+""+this.getToken();
		
		Connection cn = DriverManager.getConnection("jdbc:oracle:thin:@ordqidkdb01.dslab.ad.adp.com:2014/ikareqa","CDK_DEMO","cdk123");
		String sql;
		sql = "INSERT INTO ASSIGNED_TEST(TEST_TOKENID, TEST_TYPE, USER_ID, STATUS, USERNAME, SCORE) "
				+ "VALUES(?, ?, ?, ?, ?, ?)";
		PreparedStatement InsertObjectiveStatement = cn.prepareStatement(sql);
		InsertObjectiveStatement.setString(1, token);
		InsertObjectiveStatement.setString(2, testType);
		InsertObjectiveStatement.setString(3, user_id);
		InsertObjectiveStatement.setString(4, "P");
		InsertObjectiveStatement.setString(5, user_name);
		InsertObjectiveStatement.setInt(6, -1);
		return Integer.toString(InsertObjectiveStatement.executeUpdate());
//		
//		return Integer.toString(jdbcTemplate.update(sql, new Object[] { this.getToken(), this.getTestType().charAt(0), this.getUser_id(), "P", this.getUser_name(), -1 }));// other
	}

	public String getToken() {
		return token;
	}

	public Token() {
		super();
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_id() {
		return user_id;
	}

	@Override
	public String toString() {
		return "Token [jdbcTemplate=" + jdbcTemplate + ", token=" + token + ", user_id=" + user_id + ", user_name="
				+ user_name + ", testType=" + testType + "]";
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	public void setToken(String token) {
		this.token = token;
	}

}