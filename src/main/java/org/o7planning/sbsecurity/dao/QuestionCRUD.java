package org.o7planning.sbsecurity.dao;

import java.util.List;

import org.o7planning.sbsecurity.entity.model.Question;

public interface QuestionCRUD {
	
	//changed
	public List<Question> getAllQuestions(String type);
	int addQuestion(Question question);
	Question getQuestion(int qid, String type);
	int modifyQuestion(Question question);
}
