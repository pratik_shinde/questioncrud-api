package org.o7planning.sbsecurity.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import org.o7planning.sbsecurity.entity.model.*;

import org.o7planning.sbsecurity.dao.mapper.*;

@Component
public class studentdao implements studentInterface {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<Question> getAllQuestions() {
		List<Question> questionsList = new ArrayList();
		String sql1 = "select * from subjective";
		questionsList.addAll(jdbcTemplate.query(sql1, new studentmapperSubjective()));

		String sql2 = "select * from Objective";
		questionsList.addAll(jdbcTemplate.query(sql2, new studentmapperObjective()));

		String sql3 = "select * from programming";
		questionsList.addAll(jdbcTemplate.query(sql3, new studentmapperProgramming()));
		// Collections.shuffle(questionsList);

		return questionsList;
	}

	public List<Question> getAllQuestions(String type) {
		List<Question> questionsList = new ArrayList();
		
		String sql;
		if (type.equalsIgnoreCase("subjective") || type.equalsIgnoreCase("all")) {
			sql = "select * from subjective";
			questionsList.addAll(jdbcTemplate.query(sql, new studentmapperSubjective()));
		}
//comment

		if (type.equalsIgnoreCase("objective") || type.equalsIgnoreCase("all")) {
			sql = "select * from Objective";
			questionsList.addAll(jdbcTemplate.query(sql, new studentmapperObjective()));
		}

		if (type.equalsIgnoreCase("programming") || type.equalsIgnoreCase("all")) {
			sql = "select * from programming";
			questionsList.addAll(jdbcTemplate.query(sql, new studentmapperProgramming()));
		}
		if (questionsList.size() < 1)
			return null;

		return questionsList;
	}

}
